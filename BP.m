clear;
%三层神经网络
load('MNIST.mat');
sam_sum = 60000;
%A=train_ima(:,1)
%B=reshape(A,[28,28])
%C=zeros(size(B))
%for i=1:length(B)
%for j=1:length(B)
%C[i,j]=B[j,i];
%end
%end
%imshow(C)
%每张手写字体图片有28x28个像素点
input = 784;
output = 10;
%隐藏层神经元数量
hid = 30;
%随机初始化输入层到隐藏层的权重
w1 = randn([input,hid]);
%baocun = w1;
w2 = randn([hid,output]);
bias1 = zeros(hid,1);
bias2 = zeros(output,1);
rate1 = 0.005;
rate2 = 0.005;                
%设置学习率
%用作三层神经网络的暂时存储变量
temp1 = zeros(hid,1);
net = temp1;
temp2 = zeros(output,1);
z = temp2;
%sigmoid函数 f = 1/(e^-x + 1)   求导为 f' = f*(1-f)
%打印训练次数
for num = 1:100
    num
for i = 1:sam_sum
    label = zeros(10,1);
    label(train_lab(i)+1,1) = 1;
    %forward
    %此处选用784*1的train_ima矩阵,因此需要加(:,1)
    temp1 = train_ima(:,i)'*w1 + bias1';
    net = sigmoid(temp1);
    %此处选用hid*1的隐藏层矩阵
    temp2 = net*w2 + bias2';
    z = sigmoid(temp2);
    z = z';net = net';
    %backward
    error = label - z;
    %梯度下降
    deltaZ = error.*z.*(1-z);
    deltaNet = net.*(1-net).*(w2*deltaZ);
    for j = 1:output
        w2(:,j) = w2(:,j) + rate2*deltaZ(j).*net;
    end
    for j = 1:hid
        w1(:,j) = w1(:,j) + rate1*deltaNet(j).*train_ima(:,i);
    end
    bias2 = bias2 + rate2*deltaZ;
    bias1 = bias1 + rate1*deltaNet;
end
end