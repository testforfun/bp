test_sum = 10000;
count = 0;
for i = 1:test_sum
    temp1 = test_ima(:,i)'*w1 + bias1';
    net = sigmoid(temp1);
    %此处选用hid*1的隐藏层矩阵
    temp2 = net*w2 + bias2';
    z = sigmoid(temp2);
    [maxn,inx] = max(z);
    inx = inx -1;
    if inx == test_lab(i)
        count = count+1;
    end
end
correctRate = count/test_sum